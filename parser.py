"""
Reuters 21578 json parser module
"""

import json
import glob
import logging

from pymongo import MongoClient
from bs4 import BeautifulSoup

from dateutil.parser import parse
from dateutil.parser._parser import ParserError

LOGGER = logging.getLogger(__name__)


def open_all_files():
    """
    Opens all reuters .sgml files and returns beautifulsoup object of the file content.
    """
    for filename in glob.glob("reuters21578/reut*.sgm"):
        logging.info("Parsing file: %s", filename)
        try:
            with open(filename) as file_pointer:
                yield BeautifulSoup(file_pointer, features="html.parser")
        except UnicodeDecodeError:
            logging.warning(
                "Encoutered UnicodeDecodeError, trying to open file with latin-1 encoding"
            )
            with open(filename, encoding="latin-1") as file_pointer:
                yield BeautifulSoup(file_pointer, features="html.parser")


def parse_list_field(document, field_name):
    """
    Parses field into a list of strings
    """
    return [topic.text for topic in document.find(field_name).find_all("d")]


def parse_text_field(document):
    """
    Parses the text field
    """
    text = document.find("text")

    text_type = text.get("type")

    if text_type == "UNPROC":
        return {"type": text_type, "content": text.text}
    else:
        return {
            "type": text_type,
            "title": text.find("title").text if text.find("title") else None,
            "body": text.find("body").text if text.find("body") else None,
            "author": text.find("author").text if text.find("author") else None,
            "dateline": text.find("dateline").text if text.find("dateline") else None,
        }


def parse_date_field(datefield):
    """
    Parses the date field into a datetime object
    """
    try:
        return parse(datefield.text) if datefield.text else None
    except ParserError:
        pass


def parse_document(document):
    """
    Returns a dictionary of the beautifulsoup object
    """
    document_dict = {
        "cgisplit": document.get("cgisplit"),
        "lewissplit": document.get("lewissplit"),
        "newid": document.get("newid"),
        "oldid": document.get("oldid"),
        "has_topics": document.get("topics"),
        "datetime": parse_date_field(document.find("date")),
        "date": document.find("date").text,
        "topics": parse_list_field(document, "topics"),
        "places": parse_list_field(document, "places"),
        "people": parse_list_field(document, "people"),
        "orgs": parse_list_field(document, "orgs"),
        "companies": parse_list_field(document, "companies"),
        "text": parse_text_field(document),
    }
    return document_dict


def open_and_parse_all_documents():
    """
    Opens and parses all sgml files
    """

    for soup in open_all_files():
        for document in soup.find_all("reuters"):
            yield parse_document(document)


def main():
    """
    Dumps the parsed documents into a single json
    """

    with open("reuters21578.json", "w") as file_pointer:
        json.dump(
            list(open_and_parse_all_documents()), file_pointer, indent=4, default=str
        )


def upload_to_mongo():
    """
    Uploads parsed documents to local mongo collection
    """
    client = MongoClient()
    client.reuters.reuters21578.insert_many(open_and_parse_all_documents())


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    upload_to_mongo()
